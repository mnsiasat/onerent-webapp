import React, {useRef, useState} from 'react'
import './SearchBar.scss'
import SearchResult from '../SearchResult/SearchResult'

const SearchBar = props => {
    const [searchText, setSearchText] = useState()
    const inputRef = useRef()

    const search = event => {
        event.preventDefault()
        setSearchText(inputRef.current.value)
    }
    return (
        <React.Fragment>
            <div id="searchbar">
                <div id="cover">
                    <form method="get" action="">
                        <div className="tb">
                            <div className="td"><input ref={inputRef} type="text" placeholder="Search" required/></div>
                            <div className="td" id="s-cover">
                                <button type="submit" onClick={search}>
                                    <div id="s-circle"></div>
                                    <span></span>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <SearchResult input={searchText}/>
        </React.Fragment>
    )
}

export default SearchBar
