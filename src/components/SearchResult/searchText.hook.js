import gql from 'graphql-tag'
import { useQuery } from '@apollo/react-hooks'

export const SEARCH_TEXT = gql`
    query($input: String!){
        search(text: $input) {
            ... on User {
                firstName
                lastName
                properties {
                    street
                    city
                    state
                    zip
                    rent
                }
            }
            ... on Property {
                street
                city
                state
                zip
                rent
            }
        }
    }
`
export default options => useQuery(SEARCH_TEXT, { ...options })
