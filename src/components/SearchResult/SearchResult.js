import React, {useEffect, useState} from 'react'
import useSearchText from './searchText.hook'
import JSONPretty from 'react-json-pretty'
import './SearchResult.scss'
import { usePrevious } from "../helper/usePrevious.hooks";


const SearchResult = props => {
    const [results, setResults] = useState([])
    const {data} = useSearchText({
        variables: {input: props.input},
        skip: !props.input
    })

    const prevData = usePrevious(data);
    useEffect(() => {
        if (data) {
            setResults(data.search)
        }
    }, [data,prevData])

    return (
        <div id="results-div">
            <ol className="gradient-list">
                {results.map((item, index) => (
                    <li key={index}>
                        <JSONPretty id="json-pretty" data={item}></JSONPretty>
                    </li>
                ))}
            </ol>
        </div>
    )
}

export default SearchResult
