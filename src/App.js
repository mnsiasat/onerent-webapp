import React from 'react';
import './App.scss';
import SearchBar from './components/SearchBar/SearchBar'

function App() {
  return (
      <div id="App">
          <SearchBar/>
      </div>
  );
}

export default App;
