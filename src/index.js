import React from 'react'
import ReactDOM from 'react-dom'
import './index.scss'
import App from './App'
import * as serviceWorker from './serviceWorker'
import {createHttpLink} from 'apollo-link-http'
import {InMemoryCache, IntrospectionFragmentMatcher} from 'apollo-cache-inmemory'
import ApolloClient from 'apollo-client'
import {ApolloProvider} from '@apollo/react-hooks'

const fragmentMatcher = new IntrospectionFragmentMatcher({
    introspectionQueryResultData: {
        __schema: {
            types: [
                {
                    'kind': 'UNION',
                    'name': 'SearchResult',
                    'possibleTypes': [
                        {
                            'name': 'User'
                        },
                        {
                            'name': 'Property'
                        }
                    ]
                },
            ],
        },
    }
})


const link = createHttpLink({uri: process.env.REACT_APP_APOLLO_URL})
const cache = new InMemoryCache({fragmentMatcher})
const client = new ApolloClient({link, cache})

ReactDOM.render(
    <ApolloProvider client={client}>
        <React.StrictMode>
            <App/>
        </React.StrictMode>
    </ApolloProvider>,
    document.getElementById('root')
)


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
